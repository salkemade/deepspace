﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaveManager : MonoBehaviour
{
	public static WaveManager Instance;

	public List<Transform> spawnPrefabList = new List<Transform> ();

	public List<Transform> spawnPointList = new List<Transform> ();

	public List<Transform> targetList = new List<Transform> ();

	public float spawnInterval = 30;

	public float spawnDeviation = 10;

	public int counter = 0;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		StartCoroutine (SpawnLoop ());	
	}

	public IEnumerator SpawnLoop ()
	{
		yield return new WaitForSeconds (spawnInterval + spawnDeviation);

		Transform handle = Instantiate (spawnPrefabList [counter], spawnPointList [counter].position, spawnPointList [counter].rotation) as Transform;

		handle.GetComponent<EnemyUnitMovement> ().targetUnit = targetList [counter];

		UnitManager.Instance.AllEnemyUnits.Add (handle);

		handle.gameObject.SetActive (true);

		counter++;
		StartCoroutine (SpawnLoop ());
	}
}
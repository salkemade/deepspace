using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnitySteer;

public class MovementManager : MonoBehaviour
{
	public Ray ray;
	public RaycastHit hit;

	public LayerMask maskHorizontal;
	public LayerMask maskVertical;

	public LayerMask enemies;

	Vector3 startPos = new Vector3 ();
	Vector3 endPos = new Vector3 ();
	Vector3 endVec = new Vector3 ();

	public Transform selectionPlaneVertical;
	public Transform selectionPlaneHorizontal;

	public Vector3 targetPosition = new Vector3 ();

	public Transform verticalSelectionPlaneHelper;

	float distance = 0;

	public	List<Vector3> startLines = new List<Vector3> ();
	public 	List<Vector3> endLines = new List<Vector3> ();

	public enum States
	{
		START,
		DIRECTION,
		HEIGHT,
		ORDER,
		NONE,
		ATTACK}
	;

	public States state = States.START;

	public enum FormationType
	{
		LINE = 0,
		WALL = 1,
		SPHERE = 2,
		CLAW = 3,
		DELTA = 4,
		SWARM = 5,
		RANDOM = 6,
		NONE = 7,
		HOLDRELATIVE = 8
	}

	public FormationType formationType;

	public Transform attackTarget;




	void Update ()
	{

		//old cancel button
		if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject (-1)) {
		} else {

			if (Input.GetMouseButtonDown (0)) {
				startLines.Clear ();
				endLines.Clear ();
				state = States.START;
			}
		}


		if (UnitManager.Instance.SelectedUnits.Count > 0 && SpaceRTScamera.Instance.camState == SpaceRTScamera.CamStates.RTS) {
			
			switch (state) {
				
			case States.START:
				
				if (Input.GetMouseButtonDown (1)) {

					/*
					ray = Camera.main.ScreenPointToRay (Input.mousePosition);

					if (Physics.Raycast (ray, out hit, 10000, enemies)) {
					
						Debug.Log (" we hit an enemy give command to swarm attack it");

						Invoke ("DelayCancelAttackLine", 1);

						startLines.Clear ();
						endLines.Clear ();

						//startPos = UnitManager.Instance.selectionCenter; // UnitManager.Instance.camHolderTarget.position;

						attackTarget = hit.transform;

						foreach (Transform t in UnitManager.Instance.SelectedUnits) {
							t.GetComponent<UnitMovement> ().target = attackTarget;
							t.GetComponent<UnitMovement> ().state = UnitMovement.States.ATTACK;

							startLines.Add (t.position);
						}

						state = States.ATTACK;

					} else {
						*/
					startLines.Clear ();
					endLines.Clear ();
					
					startPos = UnitManager.Instance.camHolderTarget.position;
					
					//	DrawLine.Instance.transform.position = startPos;
					
					state = States.DIRECTION;
					//	}

				}
				
				break;
				
			case States.DIRECTION:
				
				if (Input.GetMouseButton (1)) {
					ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					
					if (Physics.Raycast (ray, out hit, 10000, maskHorizontal)) {
						endPos = hit.point;
					}
				}
				
				if (Input.GetMouseButtonUp (1)) {


					//	Vector3 targetPostition = new Vector3( target.position.x, this.transform.position.y, target.position.z ) ;
					//	this.transform.LookAt( targetPostition ) ;

					//OLD CODE
					/*
					selectionPlaneVertical.position = endPos;
					verticalSelectionPlaneHelper.LookAt (Camera.main.transform.position);
					
					selectionPlaneVertical.rotation = Quaternion.Euler (0, verticalSelectionPlaneHelper.rotation.eulerAngles.y, 0);
					selectionPlaneVertical.Rotate (0, 180, 0);
					*/

					selectionPlaneVertical.position = new Vector3 (endPos.x, Camera.main.transform.position.y, endPos.z);
					selectionPlaneVertical.LookAt (Camera.main.transform.position);
					selectionPlaneVertical.Rotate (0, 180, 0);
					state = States.HEIGHT;
					
				}
				
				break;
				
			case States.HEIGHT:
				
				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				
				if (Physics.Raycast (ray, out hit, 10000, maskVertical)) {
					endVec = new Vector3 (endPos.x, hit.point.y, endPos.z);
				}
				
				if (Input.GetMouseButtonUp (1)) {// || Input.GetMouseButtonDown (0)) {
					state = States.ORDER;
				}
				
				break;
				
			case States.ORDER:
				
				foreach (Transform t in UnitManager.Instance.SelectedUnits) {
					t.GetComponent<UnitMovement> ().isMining = false;
					t.GetComponent<UnitMovement> ().mineSequence = UnitMovement.MineSequence.HOLD;
				}
				
				SoundEffectsManager.Instance.PlayMoveVoice ();
				
				switch (formationType) {
				case FormationType.SPHERE:
					
					FibonacciSphere.Instance.CreateSphere (UnitManager.Instance.SelectedUnits.Count + 1, Vector3.one * 5 * UnitManager.Instance.SelectedUnits.Count, endVec);//see later if we need +1
					
					break;
					
					
				case FormationType.WALL:
					Wall.Instance.CreateWall (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;
					
				case FormationType.LINE:
					Line.Instance.CreateLine (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;
					
				case FormationType.DELTA:
					Delta.Instance.CreateDelta (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;
					
				case FormationType.CLAW:
					Claw.Instance.CreateClaw (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;
					
				case FormationType.HOLDRELATIVE:
					Relative.Instance.CreateRelative (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;
					
				}

				int i = 0;
				foreach (Transform t in UnitManager.Instance.SelectedUnits) {
					
					Vector3 targetPos = new Vector3 ();
					
					if (t != null) {
						//TODO dit kan allemaal nog geoptimised worden
						switch (formationType) {
						
						case FormationType.LINE:
						
						//	targetPos = endVec + (i * Vector3.left * 20);
							targetPos = Line.Instance.lineNodeList [i].position;
							t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
							t.GetComponent<UnitMovement> ().target.rotation = Line.Instance.lineNodeList [i].rotation;
							t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;

							i++;
						
							break;
						
						case FormationType.WALL:
							targetPos = Wall.Instance.wallNodeList [i].position;
							t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
							t.GetComponent<UnitMovement> ().target.rotation = Wall.Instance.wallNodeList [i].rotation;
							t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;

							i++;
						
						
							break;
						
						case FormationType.DELTA:
							targetPos = Delta.Instance.deltaNodeList [i].position;
							t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
							t.GetComponent<UnitMovement> ().target.rotation = Delta.Instance.deltaNodeList [i].rotation;
							t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;

							i++;
						
						
							break;
						
						case FormationType.CLAW:
							targetPos = Claw.Instance.clawNodeList [i].position;
							t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
							t.GetComponent<UnitMovement> ().target.rotation = Claw.Instance.clawNodeList [i].rotation;
							t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;
						
							i++;
						
						
							break;
						
						case FormationType.HOLDRELATIVE:
							targetPos = Relative.Instance.relativeNodeList [i].position;
							t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
							t.GetComponent<UnitMovement> ().target.rotation = Relative.Instance.relativeNodeList [i].rotation;
							t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;

							i++;

							break;
						case FormationType.SPHERE:
							targetPos = FibonacciSphere.Instance.sphereNodeList [i].position;// + endVec; 
							t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
							t.GetComponent<UnitMovement> ().target.rotation = FibonacciSphere.Instance.sphereNodeList [i].rotation;
							t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;

							i++;
						
							break;
						
						}

						endLines.Add (targetPos);
						startLines.Add (t.position);

						Debug.Log ("order ship(s)");
					}
				}
				
				state = States.START;
				break;

			case States.ATTACK:

				endVec = attackTarget.position;

				break;

			case States.NONE:
				
				break;

			}
		}

/*
		if (UnitManager.Instance.SelectedUnits.Count > 0 && NavigationCamera.Instance.camState == NavigationCamera.CamStates.RTS) {

			switch (state) {

			case States.START:

				if (Input.GetMouseButtonDown (1)) {
			
					startLines.Clear ();
					endLines.Clear ();

					startPos = UnitManager.Instance.camHolderTarget.position;

					//	DrawLine.Instance.transform.position = startPos;

					state = States.DIRECTION;
				}

				break;

			case States.DIRECTION:

				if (Input.GetMouseButton (1)) {
					ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				
					if (Physics.Raycast (ray, out hit, 10000, maskHorizontal)) {
						endPos = hit.point;
					}
				}

				if (Input.GetMouseButtonUp (1)) {
		
					selectionPlaneVertical.position = endPos;
					verticalSelectionPlaneHelper.LookAt (Camera.main.transform.position);
				
					selectionPlaneVertical.rotation = Quaternion.Euler (0, verticalSelectionPlaneHelper.rotation.eulerAngles.y, 0);
					selectionPlaneVertical.Rotate (0, 180, 0);
					state = States.HEIGHT;

				}

				break;

			case States.HEIGHT:

				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					
				if (Physics.Raycast (ray, out hit, 10000, maskVertical)) {
					endVec = new Vector3 (endPos.x, hit.point.y, endPos.z);
				}

				if (Input.GetMouseButtonUp (1)) {// || Input.GetMouseButtonDown (0)) {
					state = States.ORDER;
				}

				break;

			case States.ORDER:
		
				foreach (Transform t in UnitManager.Instance.SelectedUnits) {
					t.GetComponent<UnitMovement> ().isMining = false;
					t.GetComponent<UnitMovement> ().mineSequence = UnitMovement.MineSequence.HOLD;
				}

				SoundEffectsManager.Instance.PlayMoveVoice ();

				switch (formationType) {
				case FormationType.SPHERE:
						
					FibonacciSphere.Instance.CreateSphere (UnitManager.Instance.SelectedUnits.Count + 1, Vector3.one * 2 * UnitManager.Instance.SelectedUnits.Count, endVec);//see later if we need +1

					break;


				case FormationType.WALL:
					Wall.Instance.CreateWall (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;
				
				case FormationType.LINE:
					Line.Instance.CreateLine (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;

				case FormationType.DELTA:
					Delta.Instance.CreateDelta (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;

				case FormationType.CLAW:
					Claw.Instance.CreateClaw (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;

				case FormationType.HOLDRELATIVE:
					Relative.Instance.CreateRelative (endVec, UnitManager.Instance.SelectedUnits.Count);
					break;
			
				}



				int i = 0;
				foreach (Transform t in UnitManager.Instance.SelectedUnits) {

					Vector3 targetPos;




					//TODO dit kan allemaal nog geoptimised worden
					switch (formationType) {

					case FormationType.LINE:

					//	targetPos = endVec + (i * Vector3.left * 20);
						targetPos = Line.Instance.lineNodeList [i].position;
						t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
						t.GetComponent<UnitMovement> ().target.rotation = Line.Instance.lineNodeList [i].rotation;
						t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;
						endLines.Add (targetPos);
						
						startLines.Add (t.position);
						Debug.Log ("order ship(s)");
						
						i++;

						break;

					case FormationType.WALL:
						targetPos = Wall.Instance.wallNodeList [i].position;
						t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
						t.GetComponent<UnitMovement> ().target.rotation = Wall.Instance.wallNodeList [i].rotation;
						t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;
						endLines.Add (targetPos);
						
						startLines.Add (t.position);
						Debug.Log ("order ship(s)");
						
						i++;


						break;

					case FormationType.DELTA:
						targetPos = Delta.Instance.deltaNodeList [i].position;
						t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
						t.GetComponent<UnitMovement> ().target.rotation = Delta.Instance.deltaNodeList [i].rotation;
						t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;
						endLines.Add (targetPos);
						
						startLines.Add (t.position);
						Debug.Log ("order ship(s)");
						
						i++;
						
						
						break;

					case FormationType.CLAW:
						targetPos = Claw.Instance.clawNodeList [i].position;
						t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
						t.GetComponent<UnitMovement> ().target.rotation = Claw.Instance.clawNodeList [i].rotation;
						t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;
						endLines.Add (targetPos);
						
						startLines.Add (t.position);
						Debug.Log ("order ship(s)");
						
						i++;

						
						break;

					case FormationType.HOLDRELATIVE:
						targetPos = Relative.Instance.relativeNodeList [i].position;
						t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
						t.GetComponent<UnitMovement> ().target.rotation = Relative.Instance.relativeNodeList [i].rotation;
						t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;
						endLines.Add (targetPos);
						
						startLines.Add (t.position);
						Debug.Log ("order ship(s)");
						
						i++;
						
						
						break;
					case FormationType.SPHERE:
						targetPos = FibonacciSphere.Instance.sphereNodeList [i].position;// + endVec; 
						t.GetComponent<UnitMovement> ().target.position = targetPos;// endVec;
						t.GetComponent<UnitMovement> ().target.rotation = FibonacciSphere.Instance.sphereNodeList [i].rotation;
						t.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;
						endLines.Add (targetPos);
						
						startLines.Add (t.position);
						Debug.Log ("order ship(s)");
						
						i++;

						break;

					}
				}

				state = States.START;
				break;

			case States.NONE:

				break;
			}
		}
*/		
	}


	public void SetFormation (int f)
	{

		startLines.Clear ();
		endLines.Clear ();
		//set formation!
		switch (f) {
		case 0:
			formationType = FormationType.LINE;
			break;
		case 1:
			formationType = FormationType.WALL;
			break;
		case 2:
			formationType = FormationType.SPHERE;
			break;
		case 3:
			formationType = FormationType.CLAW;
			break;
		case 4:
			formationType = FormationType.DELTA;
			break;
		case 5:
			formationType = FormationType.SWARM;
			break;
		case 6:
			formationType = FormationType.RANDOM;
			break;
		case 7:
			formationType = FormationType.NONE;
			break;
		case 8:
			formationType = FormationType.HOLDRELATIVE;
			break;
		}

		endVec = UnitManager.Instance.selectionCenter;
		state = States.ORDER;

	}

	void DelayCancelAttackLine ()
	{
		state = States.START;
	}

	public void OnRenderObject ()
	{
		switch (state) {
			
		case States.START:

			//TODO not cleaned well or something?
			DrawUnitPaths ();
			break;
			
		case States.DIRECTION:
			DrawDirectionLines ();
			break;
			
		case States.HEIGHT:
			DrawHeightLines ();
			break;
			
		case States.ORDER:

			// do something with this!
			break;

		case States.ATTACK:
			DrawAttackPaths ();
			break;
		case States.NONE:
			
			break;
		}
	}

	void DrawDirectionLines ()
	{
		DrawLine.Instance.DrawSimpleLine (startPos, endPos, Color.blue);
		DrawLine.Instance.DrawCircle (50, endPos, 1.5f, Color.blue);
		distance = Vector3.Distance (startPos, endPos);
		DrawLine.Instance.DrawCircle (50, startPos, distance, Color.blue);
	}


	void DrawHeightLines ()
	{
		DrawLine.Instance.DrawSimpleLine (startPos, endPos, Color.blue);
		DrawLine.Instance.DrawSimpleLine (startPos, endVec, Color.blue);
		DrawLine.Instance.DrawSimpleLine (endPos, endVec, Color.blue);
		DrawLine.Instance.DrawCircle (50, endVec, 1.5f, Color.blue);
		distance = Vector3.Distance (startPos, endPos);
		DrawLine.Instance.DrawCircle (50, startPos, distance, Color.blue);
	}


	void DrawUnitPaths ()
	{
		if (startLines.Count > 0) {
			for (int i = 0; i < startLines.Count; ++i) {
				//TODO then it crashes look at line 565
				//	Debug.LogWarning (startLines [i]);
				if (startLines [i] != null) {
					DrawLine.Instance.DrawSimpleLine (startLines [i], endLines [i], Color.green);
					DrawLine.Instance.DrawCircle (10, endLines [i], 1.5f, Color.green);
				}
			}
		}
	}

	void DrawAttackPaths ()
	{
		Debug.Log ("line count: " + startLines.Count);
		for (int i = 0; i < startLines.Count; ++i) {
			DrawLine.Instance.DrawSimpleLine (startLines [i], attackTarget.position, Color.red);
			DrawLine.Instance.DrawCircle (10, attackTarget.position, 13.5f, Color.red);
		}

		//look at draw unit paths
		//DrawLine.Instance.DrawSimpleLine (startPos, endVec, Color.red);
		//DrawLine.Instance.DrawCircle (50, endVec, 13.5f, Color.red);

	}



}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TechTreeManager : MonoBehaviour
{

	public static TechTreeManager Instance;

	public Transform lifeSupport;
	//0
	public Transform NavigationResearch;
	//1
	public Transform Armor;
	//2
	public Transform Targeting;
	//3
	public Transform ForceField;
	//4
	public Transform Stealth;
	//5
	public Transform StarDrive;
	//6
	public Transform DNA;
	//7
	public Transform TimeTravel;
	//8
	public Transform InsanityDrive;
	//9

	public bool lifeSupport_unlocked = false;
	public bool Navigation_unlocked = false;
	public bool Armor_unlocked = false;
	public bool Targeting_unlocked = false;
	public bool ForceField_unlocked = false;
	public bool Stealth_unlocked = false;
	public bool StarDrive_unlocked = false;
	public bool DNA_unlocked = false;
	public bool TimeTravel_unlocked = false;
	public bool InsanityDrive_unlocked = false;

	public bool isResearching = false;

	float researchAmount = 0;

	public List<Transform> researchList = new List<Transform> ();

	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
		} else {
			Debug.LogError ("Tech tree manager already exists");
		}
	}

	void Start ()
	{
		researchList.Add (lifeSupport);
		//0
		researchList.Add (NavigationResearch);
		//1
		researchList.Add (Armor);
		//2
		researchList.Add (Targeting);
		//3
		researchList.Add (ForceField);
		//4
		researchList.Add (Stealth);
		//5
		researchList.Add (StarDrive);
		//6
		researchList.Add (DNA);
		//7
		researchList.Add (TimeTravel);
		//8
		researchList.Add (InsanityDrive);

	}

	/*
	public void ResearchLifeSupport ()
	{
		if (!lifeSupport_unlocked) {
			researchAmount = 0;
			StartCoroutine (LifeSupportResearch ());
		}
	}

	IEnumerator LifeSupportResearch ()
	{
		lifeSupport.GetChild (1).GetComponent<Image> ().fillAmount = researchAmount;
		yield return new WaitForSeconds (.5f);

		researchAmount += 0.01f;
		if (researchAmount < 1) {
			StartCoroutine (LifeSupportResearch ());
		} else {
			lifeSupport.GetComponent<Text> ().color = Color.green;
			NavigationResearch.GetChild (0).GetComponent<Button> ().interactable = true;
		}
	}
	*/


	public void Research (int type)
	{
		if (!lifeSupport_unlocked) {
			researchAmount = 0;
			StartCoroutine (ResearchLoop (type));
		}
	}

	IEnumerator ResearchLoop (int type)
	{
		researchList [type].GetChild (1).GetComponent<Image> ().fillAmount = researchAmount;
		yield return new WaitForSeconds (.5f);

		researchAmount += 0.01f;
		if (researchAmount < 1) {
			StartCoroutine (ResearchLoop (type));
		} else {
			researchList [type].GetComponent<Text> ().color = Color.green;

			if (type < researchList.Count - 1) {
				researchList [type + 1].GetChild (0).GetComponent<Button> ().interactable = true;
			}
		}
	}



	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

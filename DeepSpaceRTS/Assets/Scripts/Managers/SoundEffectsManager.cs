﻿using UnityEngine;
using System.Collections;

public class SoundEffectsManager : MonoBehaviour
{
	public AudioClip[] soundEffects;
	public AudioClip[] moveVoices;

	static public SoundEffectsManager Instance;


	public void PlayMoveVoice ()
	{
		GetComponent<AudioSource> ().clip = moveVoices [Random.Range (0, moveVoices.Length)];
		GetComponent<AudioSource> ().Play ();
	}


	void Awake ()
	{
		if (Instance != null) {
			Debug.LogError ("effects manager already exists");
		} else {
			Instance = this;
		}
	}

}

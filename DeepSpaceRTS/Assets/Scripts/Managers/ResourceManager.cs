﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour
{
	public static ResourceManager Instance;

	public List<Transform> Asteroids = new List<Transform> ();

	public Transform oreText;
	public float Ore = 0;

	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("Instance of resource manager already exists");
		} else {
			Instance = this;
		}
	}


	void Start ()
	{
		FindAllAsteroids ();
	}

	void Update ()
	{
		oreText.GetComponent<Text> ().text = "Ore Resources: " + Ore.ToString ();
	}

	void FindAllAsteroids ()
	{
		foreach (GameObject go in GameObject.FindGameObjectsWithTag ("Asteroid")) {
			Asteroids.Add (go.transform);
		}
	}

	public void EnableAutoResource ()
	{
		StartCoroutine (AutoResource ());
	}

	IEnumerator AutoResource ()
	{
		Ore++;
		yield return new WaitForSeconds (5);
		StartCoroutine (AutoResource ());
	}

}

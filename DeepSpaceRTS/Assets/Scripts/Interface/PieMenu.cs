﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PieMenu : MonoBehaviour
{
	public bool formationMenuEnabled = false;

	public Transform formationMenu;

	public Transform navigationButton;

	void Update ()
	{	
		if (Input.GetKeyDown (KeyCode.M)) {
			ToggleFormationMenu ();
		}

//		navigationButton.GetComponent<RectTransform> ().anchoredPosition3D = UnitManager.Instance.selectionCenter;

		if (UnitManager.Instance.SelectedUnits.Count > 0) {
			navigationButton.gameObject.SetActive (true);
			navigationButton.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (Camera.main.WorldToScreenPoint (UnitManager.Instance.selectionCenter).x, Camera.main.WorldToScreenPoint (UnitManager.Instance.selectionCenter).y);
		} else {
			navigationButton.gameObject.SetActive (false);
		}
	}

	public void ToggleFormationMenu ()
	{
		formationMenu.localPosition = new Vector3 (Input.mousePosition.x - Screen.width / 2, Input.mousePosition.y - Screen.height / 2, 0);
		formationMenuEnabled = !formationMenuEnabled;		
		formationMenu.gameObject.SetActive (formationMenuEnabled);
	}

}

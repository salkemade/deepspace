﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ContextCursor : MonoBehaviour
{

	public Texture[] cursorImages;

	public Vector3 cursorOffset;

	public Transform tooltip;

	Ray ray;

	RaycastHit hit;

	public LayerMask mask;

	public Texture currentCursor;

	public string tooltipText;

	// Use this for initialization
	void Start ()
	{
		Cursor.visible = false;

		currentCursor = cursorImages [1];
	}

	//debugging purposes only
//	void OnGUI()
//	{
//		GUI.Label();
//	}


	// Update is called once per frame
	void Update ()
	{
		/*
		transform.position = Input.mousePosition + cursorOffset;


		if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject (-1)) {
			tooltip.GetComponent<Text> ().text = "interface";
		}
		*/

		ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
		if (Physics.Raycast (ray, out hit, 100, mask)) {
			tooltipText = hit.transform.name;


			switch (hit.transform.gameObject.layer) {
			case 10:
				//enemy
				currentCursor = cursorImages [0];
				break;
			case 11:
				//player
				currentCursor = cursorImages [5];
				break;
			
			case 8:
				//horizontal
				currentCursor = cursorImages [4];
				break;
					
			case 9:
				//vertical
				currentCursor = cursorImages [4];

				break;

			default:
				currentCursor = cursorImages [1];
				break;

			}


			//	tooltip.GetComponent<Text> ().text = hit.transform.name;
		} else {
			tooltipText = "deselect";
			//	tooltip.GetComponent<Text> ().text = "deselect";
		}


	
	}


	void OnGUI ()
	{

		GUI.DrawTexture (new Rect (Input.mousePosition.x, Screen.height - Input.mousePosition.y, 40, 40), currentCursor);

		GUI.Label (new Rect (Input.mousePosition.x + 40, Screen.height - Input.mousePosition.y + 20, 200, 20), tooltipText);

	}


}

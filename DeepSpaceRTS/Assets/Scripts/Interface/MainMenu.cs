﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

	public Transform mainMenu;

	public Transform optionMenu;


	public void OpenOptionMenu ()
	{
		mainMenu.gameObject.SetActive (false);
		optionMenu.gameObject.SetActive (true);
	}

	public void CloseOptionMenu ()
	{
		mainMenu.gameObject.SetActive (true);
		optionMenu.gameObject.SetActive (false);
	}

	public void StartGame ()
	{
		SceneManager.LoadScene (1);
	}


	public void ExitGame ()
	{
		Application.Quit ();
	}

}

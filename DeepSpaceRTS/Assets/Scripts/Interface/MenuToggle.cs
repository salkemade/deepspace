﻿using UnityEngine;
using System.Collections;

public class MenuToggle : MonoBehaviour
{

	public bool toggle = false;

	public Transform menu;

	public void ToggleMenu ()
	{

		if (toggle) {
			menu.gameObject.SetActive (false);

		} else {
			menu.gameObject.SetActive (true);

		}
		toggle = !toggle;

	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SharedNodes : MonoBehaviour
{
	public static SharedNodes Instance;

	public Transform nodePrefab;

	public List<Transform> nodeList = new List<Transform> ();

	public int maxNodes = 400;

	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("shaded nodes already exists");
		} else {
			Instance = this;
		}
	}


	void Start ()
	{
		for (int i = 0; i < maxNodes; i++) {
			nodeList.Add (Instantiate (nodePrefab, transform.position, transform.rotation) as Transform);
		}
	}
}

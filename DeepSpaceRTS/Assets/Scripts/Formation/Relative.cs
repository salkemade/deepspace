﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Relative : MonoBehaviour
{

	public static Relative Instance;
	
	public List<Transform> nodeList = new List<Transform> ();
	
	public List<Transform> relativeNodeList = new List<Transform> ();

	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("Line already exists");
		} else {
			Instance = this;
		}
	}
	
	void Start ()
	{
		nodeList = SharedNodes.Instance.nodeList;
	}

	public void CreateRelative (Vector3 newpos, int size)
	{
		ClearRelative ();
		
		transform.position = Vector3.zero;
		int totalWidth = size;
	
	
		for (int i = 0; i < totalWidth; i++) {
			relativeNodeList.Add (nodeList [i]);
			nodeList [i].position = UnitManager.Instance.SelectedUnits [i].position + newpos;
			nodeList [i].parent = transform;
		}
		
		transform.position = newpos;
		transform.LookAt (UnitManager.Instance.selectionCenter);
	}
	
	public void ClearRelative ()
	{
		foreach (Transform t in relativeNodeList) {
			t.parent = null;
		}
		
		relativeNodeList.Clear ();
	}
}

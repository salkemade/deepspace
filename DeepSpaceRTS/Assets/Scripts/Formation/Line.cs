﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Line : MonoBehaviour
{

	public static Line Instance;
	
	public List<Transform> nodeList = new List<Transform> ();
	
	public List<Transform> lineNodeList = new List<Transform> ();
	
	public Transform nodePrefab;
	
	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("Line already exists");
		} else {
			Instance = this;
		}
	}
	
	void Start ()
	{
		nodeList = SharedNodes.Instance.nodeList;
	}
	
	
	public void CreateLine (Vector3 newpos, int size)
	{
		ClearLine ();
		
		transform.position = Vector3.zero;
		int totalWidth = size;

		for (int i = 0; i < totalWidth; i++) {
			lineNodeList.Add (nodeList [i]);
			nodeList [i].position = new Vector3 ((i * 20) - (totalWidth * 10), 0, 0);
			nodeList [i].parent = transform;
		}
		
		transform.position = newpos;
		transform.LookAt (UnitManager.Instance.selectionCenter);
	}
	
	public void ClearLine ()
	{
		foreach (Transform t in lineNodeList) {
			t.parent = null;
		}
		
		lineNodeList.Clear ();
	}
}

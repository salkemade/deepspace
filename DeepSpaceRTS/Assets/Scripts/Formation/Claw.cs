﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Claw : MonoBehaviour
{

	public static Claw Instance;
	
	public List<Transform> nodeList = new List<Transform> ();
	
	public List<Transform> clawNodeList = new List<Transform> ();
	
	
	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("Delta already exists");
		} else {
			Instance = this;
		}
	}
	
	void Start ()
	{
		nodeList = SharedNodes.Instance.nodeList;
	}
	
	
	public void CreateClaw (Vector3 newpos, int size)
	{
		ClearClaw ();
		
		transform.position = Vector3.zero;
		int totalWidth = size;
		
		for (int i = 0; i < totalWidth; i++) {
			
			clawNodeList.Add (nodeList [i]);
			
			if (i > size / 2) {
				nodeList [i].position = new Vector3 ((i * 10) - (totalWidth * 5), 0, ((totalWidth * 10) - (i * 10)) - (totalWidth * 5));
				//	nodeList [i].position = new Vector3 ((i * 10) - (totalWidth * 10), 0, (totalWidth * 10) - (i * 10));
			} else {
				//		nodeList [i].position = new Vector3 ((i * 10) - (totalWidth * 10), 0, (i * 10));
				nodeList [i].position = new Vector3 ((i * 10) - (totalWidth * 5), 0, ((i * 10)) - (totalWidth * 5));
			}
			
			nodeList [i].parent = transform;
		}
		
		transform.position = newpos;
		transform.LookAt (UnitManager.Instance.selectionCenter);
	}
	
	public void ClearClaw ()
	{
		foreach (Transform t in clawNodeList) {
			t.parent = null;
		}
		
		clawNodeList.Clear ();
	}
}

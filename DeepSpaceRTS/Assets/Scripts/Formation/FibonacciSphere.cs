﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FibonacciSphere : MonoBehaviour
{
	public static FibonacciSphere Instance;
	//an attempt to repurpose the script found at hhttp://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere post by Fnord
	public Transform nodePrefab;

	public List<Transform> nodeList = new List<Transform> ();

	public List<Transform> sphereNodeList = new List<Transform> ();

	public int nPoints = 20;
	
	float offset;
	float increment;

	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("fibonacci sphere already exists");
		} else {
			Instance = this;
		}
	}


	void Start ()
	{
		//	for (int i = 0; i < 400; i++) {
		//		nodeList.Add (Instantiate (nodePrefab, transform.position, transform.rotation) as Transform);
		//	}

		nodeList = SharedNodes.Instance.nodeList;

	}

	public void ClearSphere ()
	{
		foreach (Transform t in sphereNodeList) {
			t.parent = null;
		}

		sphereNodeList.Clear ();
	}
	
	public void CreateSphere (int nbrPoints, Vector3 scale, Vector3 newpos)
	{
		transform.position = Vector3.zero;
		transform.localScale = Vector3.one;
		ClearSphere ();
		offset = 2f / nbrPoints;
		increment = Mathf.PI * (3f - Mathf.Sqrt (5f));
		for (int i = 1; i <= nbrPoints; i++) {
			float y = ((i * offset) - 1) + (offset / 2);
			float r = Mathf.Sqrt (1 - Mathf.Pow (y, 2));

			float phi = i * increment;
			
			float x = Mathf.Cos (phi) * r;
			float z = Mathf.Sin (phi) * r;
			Vector3 pos = new Vector3 (x, y, z);

			if (!System.Single.IsNaN (pos.x) && !System.Single.IsNaN (pos.y) && !System.Single.IsNaN (pos.z)) {
				nodeList [i].position = pos;// + newpos;
				nodeList [i].parent = transform;
				sphereNodeList.Add (nodeList [i]);
			}
			//	Instantiate (node, pos, Quaternion.identity);
		}
	
		transform.localScale = scale;


		transform.position = newpos;
	}

}

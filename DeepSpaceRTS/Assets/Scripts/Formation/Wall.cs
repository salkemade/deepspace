﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wall : MonoBehaviour
{
	public static Wall Instance;

	public List<Transform> nodeList = new List<Transform> ();

	public List<Transform> wallNodeList = new List<Transform> ();

	public Transform nodePrefab;

	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("Wall already exists");
		} else {
			Instance = this;
		}
	}

	void Start ()
	{
		nodeList = SharedNodes.Instance.nodeList;
	}


	public void CreateWall (Vector3 newpos, int size)
	{
	
		ClearWall ();

		transform.position = Vector3.zero;

		float width = Mathf.Sqrt (size);

		int totalWidth = (int)width;
		Debug.Log ("width: " + totalWidth);

		int k = 0;

		for (int i = 0; i < totalWidth+1; i++) {
			for (int j = 0; j < totalWidth+1; j++) {
				wallNodeList.Add (nodeList [k]);
				nodeList [k].position = new Vector3 ((i * 20) - (totalWidth * 10), (j * 20) - (totalWidth * 10), 0);
				nodeList [k].parent = transform;
				k++;
			}
		}

		transform.position = newpos;
		transform.LookAt (UnitManager.Instance.selectionCenter);
	}

	public void ClearWall ()
	{
		foreach (Transform t in wallNodeList) {
			t.parent = null;
		}

		wallNodeList.Clear ();
	}

}

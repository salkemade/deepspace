﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//using System.Collections.Generic;

public class Asteroid : MonoBehaviour
{
	public float minRotation = -2;
	public float maxRotation = 2;
	Vector3 randRotation;

	public Transform statusBar;

	public float unitsOre = 50;

	public float startUnitsEnergy = 0;

	public Transform minePos;
	//bool isSelected = false;
	public Transform visualAsteroid;

	void Start ()
	{
		startUnitsEnergy = unitsOre;

		randRotation = new Vector3 (Random.Range (minRotation, maxRotation), Random.Range (minRotation, maxRotation), Random.Range (minRotation, maxRotation));
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		visualAsteroid.Rotate (randRotation);
	}

	public void DepleteOre (float amount, Transform miner)
	{
		if (unitsOre > 1) {
			unitsOre -= amount;
			float scale = unitsOre / startUnitsEnergy;
			statusBar.GetChild (0).GetChild (0).GetComponent<Image> ().fillAmount = scale;
			transform.localScale = new Vector3 (scale, scale, scale);

			//temp solution moet eerst nog een rondje terug
			//	ResourceManager.Instance.Energy++;

			miner.GetComponent<TurretMiner> ().AddOre (amount);

		} else {
			RemoveAsteroid ();
		}
	}


	void RemoveAsteroid ()
	{
		ResourceManager.Instance.Asteroids.Remove (transform);

		//Destroy (gameObject);
		gameObject.SetActive (false);
	}

	public void ShowStatusBar ()
	{
		statusBar.gameObject.SetActive (true);
	}

	public void HideStatusBar ()
	{
		statusBar.gameObject.SetActive (false);
	}

	void OnMouseOver ()
	{
		ShowStatusBar ();

		if (Input.GetMouseButtonDown (1)) {
			foreach (Transform t in	UnitManager.Instance.SelectedUnits) {
				if (t.GetComponent<UnitMovement> ().type == UnitMovement.ShipType.MINER) {
					t.GetComponent<UnitMovement> ().tMiner.GetComponent<TurretMiner> ().targetAsteroid = transform;
					t.GetComponent<UnitMovement> ().tMiner.GetComponent<TurretMiner> ().isMining = true;
					t.GetComponent<UnitMovement> ().currentAsteroid = transform;
					t.GetComponent<UnitMovement> ().mineSequence = UnitMovement.MineSequence.GOMINE;
				}
			}
		}

	}



	void OnMouseExit ()
	{
		//	if (Input.GetMouseButtonDown (0)) {
		HideStatusBar ();
		//	}
	}
}

﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class NavigationCamera : MonoBehaviour
{
	public static NavigationCamera Instance;

	public Transform target;
	public float distance = 15.0f;
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;
	
	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;
	
	public float distanceMin = 10.5f;
	public float distanceMax = 150f;
	
	public float smoothTime = 2f;
	
	float rotationYAxis = 0.0f;
	float rotationXAxis = 0.0f;
	
	float velocityX = 0.0f;
	float velocityY = 0.0f;
	
	//Rect mouserec;// = new Rect ();

	//float x = 0.0f;
	//float y = 0.0f;

	float xSpeedRTS = 0;
	float ySpeedRTS = 0;
	
	public Transform originalTransform;

	public float RTSmoveSpeed = 2;

	public int screenBorderMargin = 10;

	public Transform selectionPlaneHorizontal;

	public enum CamStates
	{
		ORBIT,
		RTS,
		STATIC,
		MENU,
		CUTSCENE

	}
	;
	Vector3 startVector;

	public CamStates camState;

	void Awake ()
	{
		Instance = this;
	}


	// Use this for initialization
	void Start ()
	{
		startVector = transform.position;

		Vector3 angles = transform.eulerAngles;
		rotationYAxis = angles.y;
		rotationXAxis = angles.x;
		
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody> ()) {
			GetComponent<Rigidbody> ().freezeRotation = true;
		}

		//	mouserec = new Rect (20, 20, Screen.width - 20, Screen.height - 20);

		//	Vector3 angles = transform.eulerAngles;
		//	x = angles.y;
		//	y = angles.x;
		
		//	rigidbody = GetComponent<Rigidbody> ();
	}
	
	void LateUpdate ()
	{
		if (UnitManager.Instance.SelectedUnits.Count == 0) {
			camState = CamStates.RTS;
			//target = null;
		} 

		/*
		if (Input.GetMouseButton (1) && Input.GetMouseButton (0)) {
			camState = CamStates.ORBIT;
		} else {
			camState = CamStates.RTS;
		}
		*/

		switch (camState) {
		case CamStates.ORBIT:
			//OrbitCam ();
			Camera.main.transform.GetComponent<DragMouseOrbit> ().enabled = true;
			break;

		case CamStates.RTS:
			RTSCam ();
			break;

		case CamStates.MENU:
			MenuCam ();
			break;

		case CamStates.CUTSCENE:
			CutsceneCam ();
			break;

		case CamStates.STATIC:
			//do nuffin
			break;
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			if (camState == CamStates.RTS) {
				camState = CamStates.ORBIT;
			} else {
				camState = CamStates.RTS;
				Camera.main.transform.GetComponent<DragMouseOrbit> ().enabled = false;
			}
		}
	}

	void MenuCam ()
	{

	}

	void RTSCam ()
	{
		//voeg scroll mouse toe
		Vector3 dir = Camera.main.transform.TransformDirection (Vector3.forward) * Input.GetAxis ("Mouse ScrollWheel") * 5;
		transform.Translate (dir * 1);

		Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, originalTransform.position, .1f);
		Camera.main.transform.rotation = Quaternion.Lerp (Camera.main.transform.rotation, originalTransform.rotation, .1f);


		if (Input.GetKeyDown (KeyCode.H)) {
			transform.position = startVector;
		}

		// eventueel hier ook acceleratie inbouwen LERP?
		if (Input.mousePosition.x < screenBorderMargin) {

			xSpeedRTS = Mathf.Lerp (xSpeed, RTSmoveSpeed, .001f);

			transform.Translate (-xSpeedRTS * Time.deltaTime, 0, 0);
		}
		
		if (Input.mousePosition.x > Screen.width - screenBorderMargin) {

			xSpeedRTS = Mathf.Lerp (xSpeed, RTSmoveSpeed, .001f);

			transform.Translate (xSpeedRTS * Time.deltaTime, 0, 0);
		}



		if (Input.mousePosition.y < screenBorderMargin) {
		
			ySpeedRTS = Mathf.Lerp (ySpeedRTS, RTSmoveSpeed, 0.001f);

			transform.Translate (0, 0, -RTSmoveSpeed * Time.deltaTime);
		}
		
		
		if (Input.mousePosition.y > Screen.height - screenBorderMargin) {

			ySpeedRTS = Mathf.Lerp (ySpeedRTS, RTSmoveSpeed, 0.001f);

			transform.Translate (0, 0, RTSmoveSpeed * Time.deltaTime);
		}


		//	if (Input.mouseScrollDelta != 0) {

	

		//	}


		/*
		// eventueel hier ook acceleratie inbouwen LERP?
		if (Input.mousePosition.x < screenBorderMargin) {
			transform.Translate (- RTSmoveSpeed * Time.deltaTime, 0, 0);
		}

		if (Input.mousePosition.x > Screen.width - screenBorderMargin) {
			transform.Translate (RTSmoveSpeed * Time.deltaTime, 0, 0);
		}

		if (Input.mousePosition.y < screenBorderMargin) {
			transform.Translate (0, 0, -RTSmoveSpeed * Time.deltaTime);
		}


		if (Input.mousePosition.y > Screen.height - screenBorderMargin) {
			transform.Translate (0, 0, RTSmoveSpeed * Time.deltaTime);
		}
		*/
	}


	void CutsceneCam ()
	{

	}

	void OrbitCam ()
	{
		if (target) {

			//	if (!mouserec.Contains (Input.mousePosition)) {
			if (Input.GetMouseButton (1) && Input.GetMouseButton (0)) {
					
				//if (!mouserec.Contains (Input.mousePosition)) {
				velocityX += xSpeed * Input.GetAxis ("Mouse X") * distance * 0.02f;
				velocityY += ySpeed * Input.GetAxis ("Mouse Y") * 0.02f;
				//}
			}
			//	}

			rotationYAxis += velocityX;
			rotationXAxis -= velocityY;
				
			rotationXAxis = ClampAngle (rotationXAxis, yMinLimit, yMaxLimit);
				
//			Quaternion fromRotation = Quaternion.Euler (transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
			Quaternion toRotation = Quaternion.Euler (rotationXAxis, rotationYAxis, 0);
			Quaternion rotation = toRotation;
				
			distance = Mathf.Clamp (distance - Input.GetAxis ("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
				
			RaycastHit hit;
			if (Physics.Linecast (target.position, transform.position, out hit)) {
				distance -= hit.distance;
			}

			Vector3 negDistance = new Vector3 (0.0f, 0.0f, -distance);
			Vector3 position = rotation * negDistance + target.position;
				
			transform.rotation = rotation;
			transform.position = position;
				
			velocityX = Mathf.Lerp (velocityX, 0, Time.deltaTime * smoothTime);
			velocityY = Mathf.Lerp (velocityY, 0, Time.deltaTime * smoothTime);
		}
	}

	
	public static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp (angle, min, max);
	}




}

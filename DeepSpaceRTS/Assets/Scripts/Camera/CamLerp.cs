﻿using UnityEngine;
using System.Collections;

public class CamLerp : MonoBehaviour
{

	public Transform target;

	public float rotSpeed = 0.1f;

	public float movSpeed = 0.1f;

	void LateUpdate ()
	{
	
		if (!Input.GetMouseButton (0)) {
			if (!Input.GetMouseButton (1)) {
				transform.position = Vector3.Lerp (transform.position, target.position, movSpeed);
				transform.rotation = Quaternion.Lerp (transform.rotation, target.rotation, rotSpeed);
			}
		}
	}
}

using UnityEngine;
using System.Collections;

public class OrbitCamera : MonoBehaviour
{

	public Transform target;
	public float rotateSpeed = 3.0f;

	float distance;
	float _x;
	float _y;
	float timer;
	float scrollWheel;

	public float scrollWheelSpeed = 30;

	void Start ()
	{
		distance = 6;
		_y = 45;

		RotateCamera ();
		transform.LookAt (target);
	}

	void Update ()
	{
		scrollWheel = Input.GetAxis ("Mouse ScrollWheel");
		
		timer += Time.deltaTime;

		/*
		distance = scrollWheel * scrollWheelSpeed;

		if (distance > 200) {
			distance = 200;
		}

		if (distance < 10) {
			distance = 10;
		}
		*/
		if (scrollWheel > 0 && timer > 0.01f) {
			timer = 0;
			distance -= 0.5f * scrollWheelSpeed;

			if (distance <= 2) {
				distance = 2;
			}

			//	RotateCamera ();
		}

		if (scrollWheel < 0 && timer > 0.01f) {
			timer = 0;
			distance += 0.5f * scrollWheelSpeed;

			if (distance >= 500) {
				distance = 500;
			}

			//	RotateCamera ();
		}



		if (Input.GetMouseButton (2)) {

			//target = UnitManager.Instance.selectionCenter

			_x += Input.GetAxis ("Mouse X") * rotateSpeed;
			_y -= Input.GetAxis ("Mouse Y") * rotateSpeed;
			
		
		}

	
		RotateCamera ();


	}

	void RotateCamera ()
	{
		Quaternion rotation = Quaternion.Euler (_y, _x, 0);
		Vector3 position = rotation * new Vector3 (0.0f, 0.0f, -distance) + UnitManager.Instance.selectionCenter;// target.position;
		transform.rotation = rotation;
		transform.position = position;
	}
}

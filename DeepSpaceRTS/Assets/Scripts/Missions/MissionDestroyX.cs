﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissionDestroyX : MonoBehaviour
{
	public bool missionSucceeded = false;
	
	public bool missionActive = false;

	public List<Transform> killList = new List<Transform> ();
	
	public float minDistance = 50;
	
	public string missionMsg;
	
	public bool activeFromBeginnig = false;

	public int kills = 0;

	public int startEnemies;

	void Start ()
	{

		startEnemies = killList.Count;

		missionMsg = "Kill all enemy ships!";

		if (activeFromBeginnig) {
			StartCoroutine (CheckSucceedLoop ());
		}
		
		Debug.Log (missionMsg);
	}

	public void StartMission ()
	{
		StartCoroutine (CheckSucceedLoop ());
	}

	IEnumerator CheckSucceedLoop ()
	{
		missionActive = true;

		if (kills == startEnemies || killList.Count == 0) {
			missionSucceeded = true;
			MissionManager.Instance.NextMission (transform);
		} else {

			bool killListStillFull = false;


			int count = 0;
			foreach (Transform t in killList) {
			
				if (t == null) {



					killList.RemoveAt (count);
					kills++;
					//	killList.rem
					killList.Remove (t);
				} else {
					killListStillFull = true;
				}


				if (!killListStillFull) {
					missionSucceeded = true;
					if (MissionManager.Instance.missionList.Count > 0) {
						MissionManager.Instance.NextMission (transform);
					} else {
						Debug.Log ("all missions completed");
					}
				}

				count++;
			}
		
			Debug.Log ("has not reached goal yet");
		}
		
		yield return new WaitForSeconds (1);
		
		if (!missionSucceeded) {
			if (this != null)
				StartCoroutine (CheckSucceedLoop ());
		}
	}

	void OnDestroy ()
	{
		StopAllCoroutines ();
	}

}

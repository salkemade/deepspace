﻿using UnityEngine;
using System.Collections;

public class MissionReachX : MonoBehaviour
{

	public bool missionSucceeded = false;

	public bool missionActive = false;

	public Transform endGoal;
	public Transform ship;

	public float minDistance = 50;

	public string missionMsg;

	public bool activeFromBeginnig = false;

	void Start ()
	{
		missionMsg = "Get your " + ship.gameObject.name + " to " + endGoal.gameObject.name + ".";

		if (activeFromBeginnig) {
			StartCoroutine (CheckSucceedLoop ());
		}

		Debug.Log (missionMsg);
	}

	public void StartMission ()
	{
		StartCoroutine (CheckSucceedLoop ());
	}
	
	IEnumerator CheckSucceedLoop ()
	{
		missionActive = true;

		if (Vector3.Distance (ship.position, endGoal.position) < minDistance) {
			missionSucceeded = true;
			MissionManager.Instance.NextMission (transform);
		} else {
			Debug.Log ("has not reached goal yet");
		}

		yield return new WaitForSeconds (1);

		if (!missionSucceeded)
			StartCoroutine (CheckSucceedLoop ());
	}
}

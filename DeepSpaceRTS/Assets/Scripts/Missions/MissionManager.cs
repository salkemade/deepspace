﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.SceneManagement;

public class MissionManager : MonoBehaviour
{

	public static MissionManager Instance;

	public List<Transform> missionList = new List<Transform> ();

	public Transform currentMission;

	public int nextLevel;

	void Awake ()
	{
		if (Instance != null) {
			Debug.Log ("Instance already created");
		} else {
			Instance = this;
		}
	}

	void Start ()
	{
		currentMission = missionList [missionList.Count - 1];
	}

	public void NextMission (Transform self)
	{
		missionList.Remove (self);
		//	Destroy (self.gameObject);

		if (missionList.Count == 0) {
			NextLevel ();
		} else {
			Debug.Log ("level completed");
		}

		gameObject.SetActive (false);
	}

	public void NextLevel ()
	{
		Debug.Log ("level completed");
		SceneManager.LoadScene (nextLevel);
		//Application.LoadLevel (nextLevel);
	}


}

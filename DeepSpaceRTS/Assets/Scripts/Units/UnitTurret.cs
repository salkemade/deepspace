﻿using UnityEngine;
using System.Collections;

public class UnitTurret : MonoBehaviour
{

	public float damage = 10;
	
	public float range = 150;
	public Transform targetUnit;
	
	void Start ()
	{
		StartCoroutine (CheckForNearUnits ());
	}
	
	
	IEnumerator CheckForNearUnits ()
	{
		
		yield return new WaitForSeconds (1);
		
		targetUnit = GetClosestUnits (UnitManager.Instance.AllEnemyUnits.ToArray ());
		
		if (targetUnit != null) {
			
			if (Vector3.Distance (transform.position, targetUnit.position) < range) {
				
				GetComponent<LineRenderer> ().SetPosition (0, transform.position);
				GetComponent<LineRenderer> ().SetPosition (1, targetUnit.position);
				GetComponent<LineRenderer> ().enabled = true;
				
				targetUnit.GetComponent<UnitStats> ().DoDamage (damage);	
				
			} else {
				GetComponent<LineRenderer> ().enabled = false;
			}
		}
		StartCoroutine (CheckForNearUnits ());
	}

	Transform GetClosestUnits (Transform[] enemies)
	{
		Transform tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (Transform t in enemies) {
			if (t != null) {
				float dist = Vector3.Distance (t.position, currentPos);
				if (dist < minDist) {
					tMin = t;
					minDist = dist;
				}
			}
		}
		return tMin;
	}
}
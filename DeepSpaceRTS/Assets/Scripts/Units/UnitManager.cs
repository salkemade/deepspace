﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitManager : MonoBehaviour
{
	public List<Transform> AllSelectableUnits = new List<Transform> ();

	public List<Transform> SelectedUnits = new List<Transform> ();

	public List<Transform> AllEnemyUnits = new List<Transform> ();

	public static UnitManager Instance;

	public List<Transform> carriers = new List<Transform> ();
	public List<Transform> fighters = new List<Transform> ();
	public List<Transform> miners = new List<Transform> ();

	public Transform selectionPlaneHorizontal;

	public Vector3 selectionCenter;
	public float selectionRadius;
	public Transform camHolderTarget;

	public  int currentSelected = 0;

	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
		} else {
			Debug.LogWarning ("Instance of Unitmanager already exists");
		}
	}

	void Start ()
	{
		GameObject[] allStartObjects = GameObject.FindGameObjectsWithTag ("PlayerUnit");

		foreach (GameObject go in allStartObjects) {
			AllSelectableUnits.Add (go.transform);
		}

		GameObject[] allStartEnemyObjects = GameObject.FindGameObjectsWithTag ("EnemyUnit");
		
		foreach (GameObject go in allStartEnemyObjects) {
			AllEnemyUnits.Add (go.transform);
		}

		StartCoroutine (CleanUpLoop ());
	}


	IEnumerator CleanUpLoop ()
	{

		yield return new WaitForSeconds (2);

		CleanUpLists ();

		StartCoroutine (CleanUpLoop ());
	}

	void ClearSelected ()
	{
		foreach (Transform t in UnitManager.Instance.SelectedUnits) {

			t.GetComponent<Selected> ().DeSelect ();

		}
		UnitManager.Instance.SelectedUnits.Clear ();
	}

	public void SelectNextUnit ()
	{
		ClearSelected ();

		SelectedUnits.Add (AllSelectableUnits [currentSelected]);
		AllSelectableUnits [currentSelected].GetComponent<Selected> ().Select ();

		if (currentSelected == AllSelectableUnits.Count - 1) {
			currentSelected = 0;

		} else {
			currentSelected++;
		}


	}

	public void SelectPreviousUnit ()
	{
		
		ClearSelected ();

		SelectedUnits.Add (AllSelectableUnits [currentSelected]);
		AllSelectableUnits [currentSelected].GetComponent<Selected> ().Select ();

		//	if (currentSelected == SelectedUnits.Count) {
		//		currentSelected = 0;
		//}

		if (currentSelected == 0) {
			currentSelected = AllSelectableUnits.Count - 1;
		} else {
			currentSelected--;
		}
	

	}

	void CleanUpLists ()
	{
		
		foreach (Transform t in AllEnemyUnits) {
			if (t == null) {
				AllEnemyUnits.Remove (t);
			}
		}

		/*
		foreach (Transform t in AllSelectableUnits) {
			if (t == null) {
				AllSelectableUnits.Remove (t);
			}
		}
		*/
	}

	void Update ()
	{
		SelectionCenter ();

		if (SelectedUnits.Count > 1) {
			selectionRadius = SelectionRadiusFlat ();
		} else if (SelectedUnits.Count == 1) {
			selectionRadius = 10;
		}
	}

	void SelectionCenter ()
	{
		if (SelectedUnits.Count == 1) {
			selectionCenter = SelectedUnits [0].position;
			selectionPlaneHorizontal.position = selectionCenter;
			camHolderTarget.position = selectionCenter;
		} else if (SelectedUnits.Count > 1) {
			Vector3 allpoints = Vector3.zero;
			
			foreach (Transform t in SelectedUnits) {
				allpoints += t.position;
			}
			
			Vector3 centrePoint = allpoints / SelectedUnits.Count;
			selectionCenter = centrePoint;
			selectionPlaneHorizontal.position = selectionCenter;
			camHolderTarget.position = selectionCenter;
		}
	}

	float SelectionRadiusFlat ()
	{
		List<float> x = new List<float> ();

		for (int i = 0; i < SelectedUnits.Count; ++i) {
			x.Add (SelectedUnits [i].position.x);
		}

		List<float> y = new List<float> ();
		
		for (int i = 0; i < SelectedUnits.Count; ++i) {
			y.Add (SelectedUnits [i].position.y);
		}

		float Minx = Min (x.ToArray ());
		float Miny = Min (y.ToArray ());
		float Maxx = Max (x.ToArray ());
		float Maxy = Max (y.ToArray ());

		float distance = Vector2.Distance (new Vector2 (Minx, Miny), new Vector2 (Maxx, Maxy));

		return distance / 2;
	}

	
	private float Min (float[] numbers)
	{
		float m = numbers [0];
		
		for (int i = 0; i < numbers.Length; i++)
			if (m > numbers [i])
				m = numbers [i];
		
		return m;
	}

	private float Max (float[] numbers)
	{
		float m = numbers [0];
		
		for (int i = 0; i < numbers.Length; i++)
			if (m > numbers [i])
				m = numbers [i];
		
		return m;
	}


}

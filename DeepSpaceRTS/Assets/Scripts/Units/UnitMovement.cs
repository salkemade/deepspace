﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnitySteer.Behaviors;

[RequireComponent (typeof(AutonomousVehicle)), RequireComponent (typeof(SteerForPoint))]
public class UnitMovement : MonoBehaviour
{

	public SteerForPoint _steering;

	public SteerForPursuit _pursuit;

	public SteerForTether _thether;

	bool nextPoint = false;

	public Transform target;
	public Transform targetHelper;

	public float movementRadius = 10;
	public float rotationSpeed = .5f;
	public float movementSpeed = 4f;

	private float movespeed;
	public Transform turretMiner;

	public List<Transform> nodes = new List<Transform> ();
	private float rotSpeed;
	int nodeIterator = 0;

	public Transform tMiner;

	public enum MineSequence
	{
		GOMINE,
		GODOCK,
		GOSHIP,
		GOOUT,
		HOLD
	}

	public enum ShipType
	{
		MINER,
		FIGHTER,
		CARRIER,
		FRIGATE,
		BATTLESHIP,
		STEALTH,
		STATION,
		WATCHTOWER
	}

	public enum States
	{
		HOLD,
		MOVINGTOPOINT,
		ORDER,
		DEFEND,
		ATTACK,
		DESTROYED,
		DOCK,
		UNDOCK,
		MOVINGNODEPATH,
		MOVINGFORMATION}
	;

	public Transform currentAsteroid;
	public MineSequence mineSequence;
	public ShipType type;
	public States state;

	public bool isMining = false;

	void Start ()
	{
		_steering = GetComponent<SteerForPoint> ();
		//	_steering.OnArrival += (_) => FindNewTarget();
		//	FindNewTarget();

		rotSpeed = rotationSpeed;
		movespeed = movementSpeed;
		target.parent = null;
	}

	void Update ()
	{
		Move ();
	}

	void Move ()
	{
		switch (state) {
		case States.HOLD:
			break;
			
			
		case States.MOVINGTOPOINT:
		
			MovingToPoint ();
			break;

		case States.DOCK:
		//	Docking ();

			break;

		case States.ATTACK:

			// DO attack stuff here
			Attacking ();
			break;


		case States.MOVINGNODEPATH:

			StartCoroutine (FollowNodalPAth ());

			break;

		}
	}


	void Docking ()
	{
		transform.position = Vector3.Lerp (transform.position, target.position, .002f);
		transform.rotation = Quaternion.Lerp (transform.rotation, target.rotation, .01f);

		if (target.name == "dockingbay") {
			//Debug.Log ("commense docking sequence");
			//state = States.DOCK;
			if (Vector3.Distance (transform.position, target.position) < 15) {
				ResourceManager.Instance.Ore += turretMiner.GetComponent<TurretMiner> ().minedOre;
				turretMiner.GetComponent<TurretMiner> ().minedOre = 0;
			
				//	StartCoroutine (UnDockMiner ());
			}
		}
	}




	IEnumerator FollowNodalPAth ()
	{
		if (GetComponent<UnitNodes> () != null) {
			if (GetComponent<UnitNodes> ().nodes.Count > 0) {
				
				target = GetComponent<UnitNodes> ().nodes [nodeIterator];
				if (Vector3.Distance (transform.position, target.position) < 10) {
					//GetComponent<UnitNodes> ().nodes.RemoveAt (0);
					nodeIterator++;
				}
				if (nodeIterator == GetComponent<UnitNodes> ().nodes.Count) {
					GetComponent<UnitNodes> ().nodes.Clear ();
				}
			}
		}

		yield return nextPoint;

		StartCoroutine (FollowNodalPAth ());

	}

	void SetHold ()
	{
		state = States.HOLD;
	}

	void Attacking ()
	{
		//_steering.enabled = false;

		if (target != null) {
			_pursuit.Quarry = target.GetComponent<AutonomousVehicle> ();
			_pursuit.enabled = true;
		} else {
			//cancel TODO
		}
	}

	void MovingToPoint ()
	{
		//	_thether.enabled = false;
		//	_pursuit.enabled = false;
		switch (type) {

		case ShipType.CARRIER:

			if (_steering.ReportedArrival) {
				SetHold ();
			}

			break;
			
		case ShipType.FIGHTER:

			if (_steering.ReportedArrival) {
				SetHold ();
			}

			break;
			
		case ShipType.MINER:

			if (isMining) {
				switch (mineSequence) {
				case MineSequence.HOLD:
					target.position = transform.position;

					break;
				case MineSequence.GODOCK:
					if (Vector3.Distance (transform.position, target.position) > 10) {
						target.position = UnitManager.Instance.carriers [0].GetComponent<UnitCarrier> ().dockingBay.position;
					} else {
						tMiner.GetComponent<TurretMiner> ().DumpOre ();
						mineSequence = MineSequence.GOMINE;
						//Debug.LogError ("STOP");
					}
					break;
				case MineSequence.GOOUT:
					if (Vector3.Distance (transform.position, target.position) > 10) {
						target.position = UnitManager.Instance.carriers [0].GetComponent<UnitCarrier> ().wayPointOut.position;

					} else {
						target.position = UnitManager.Instance.carriers [0].GetComponent<UnitCarrier> ().dockingBay.position;
						mineSequence = MineSequence.GODOCK;
					}
					break;
				case MineSequence.GOSHIP:
					target.position = UnitManager.Instance.carriers [0].position;

					break;

				case MineSequence.GOMINE:
					if (currentAsteroid != null) {
						target.position = currentAsteroid.GetComponent<Asteroid> ().minePos.position;
					} else {
						mineSequence = MineSequence.HOLD;
					}
					break;
				}

			}

			break;
			
		case ShipType.STATION:
			
			break;
			
		}

		_steering.TargetPoint = target.position;

		Debug.Log ("Steering enabled");

		_steering.enabled = true;
	
		if (_steering.ReportedArrival) {
			SetHold ();
		}
	}

	void OnDestroy ()
	{
		if (GetComponent<Selected> ().isSelected) {
			UnitManager.Instance.SelectedUnits.Remove (transform);
		}

		UnitManager.Instance.AllSelectableUnits.Remove (transform);
	}


}

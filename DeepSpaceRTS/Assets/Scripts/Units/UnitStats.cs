﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnitStats : MonoBehaviour
{
	public float targetingRange = 0;
	public float hitPoints = 0;
	private float initialHitPoints;
	public float speedMultiplier = 0 ;
	public float damageReduction = 0;
	public float attackAccuracy = 0;
	public float damagePoints = 0;

	public Transform healthBar;

	public Transform deadExplosionPrefab;
	public float explosionMultiplier = 1;

	public Transform status;

	void Start ()
	{
		healthBar = status.GetChild (0);
		initialHitPoints = hitPoints;
	}

	public void DoDamage (float hp)
	{
		if (hitPoints > 0) {
			hitPoints -= hp;

			healthBar.GetComponent<Image> ().fillAmount = hitPoints / initialHitPoints;

			healthBar.GetComponent<Image> ().color = Color.Lerp (healthBar.GetComponent<Image> ().color, Color.red, .8f - (hitPoints / initialHitPoints));

		} else {
			//quick fix for now 
			Transform explosion = Instantiate (deadExplosionPrefab, transform.position, transform.rotation) as Transform;
			explosion.GetComponent<ParticleSystemMultiplier> ().multiplier = explosionMultiplier;
		 
			if (UnitManager.Instance.SelectedUnits.Remove (transform)) {
				Debug.Log ("removed");
			} else {
				Debug.LogWarning ("object not removed when told, probably already dead");
			}

			if (status != null)
				Destroy (status.gameObject);

			Destroy (gameObject, .1f);
		}
	}
/*
	void OnMouseEnter ()
	{
	
			healthBar.parent.gameObject.SetActive (true);
	}

	void OnMouseExit ()
	{
			healthBar.parent.gameObject.SetActive (false);
	}
*/

	/*
	void OnMouseDown ()
	{
		if (tag == "PlayerUnit")
			UnitManager.Instance.SelectedUnits.Add (transform);
	}
*/
	void Update ()
	{
		
	}
}

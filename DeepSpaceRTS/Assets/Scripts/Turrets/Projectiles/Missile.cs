﻿using UnityEngine;
using System.Collections;

public class Missile : MonoBehaviour
{
	public Transform target;
	public Transform helper;

	public float interceptSpeed = .02f;
	public float speed = 1;
	public LayerMask mask;
	Ray ray;
	RaycastHit hit;
	float damage = 40;

	void Start ()
	{
		//helper.parent = null;
	}

	void Update ()
	{
		ray.direction = transform.TransformDirection (Vector3.forward);
		ray.origin = transform.position;
		if (target == null) {
			//Destroy (helper.gameObject);
			//	Destroy (gameObject);
			helper.gameObject.SetActive (false);
			gameObject.SetActive (false);
		}

		if (Physics.Raycast (ray, out hit, 5, mask)) {
			if (hit.transform != null) {
				if (hit.transform.GetComponent<UnitStats> () != null) {
					hit.transform.GetComponent<UnitStats> ().DoDamage (damage);	
					//Destroy (helper.gameObject);
					Destroy (gameObject);
				} 
			} 

		} else {

			//helper.LookAt (target.position);
			//	transform.rotation = Quaternion.Lerp (transform.rotation, target.rotation, interceptSpeed);

			if (target != null)
				transform.LookAt (target.position);

			transform.Translate (Vector3.forward * speed);
		}
	}
}

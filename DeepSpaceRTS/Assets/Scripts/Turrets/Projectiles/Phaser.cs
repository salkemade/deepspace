﻿using UnityEngine;
using System.Collections;

public class Phaser : MonoBehaviour
{

	Ray ray;

	RaycastHit hit;
	public LayerMask mask;
	public float speed = 5;

	public float damage = 20;
	// Use this for initialization
	void Start ()
	{
		Destroy (gameObject, 8);
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		ray.origin = transform.position;
		ray.direction = transform.TransformDirection (Vector3.back);
		if (Physics.Raycast (ray, out hit, 35, mask)) {
			//do damage
			//Debug.LogError ("hit!");
			if (hit.transform != null) {
				if (hit.transform.GetComponent<UnitStats> () != null) {
					hit.transform.GetComponent<UnitStats> ().DoDamage (damage);	
					Destroy (gameObject);
				} 
			} 
		} else {
			transform.Translate (Vector3.forward * speed);
		}
	}
}

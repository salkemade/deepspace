﻿using UnityEngine;
using System.Collections;

public class TurretLaser : MonoBehaviour
{

	public float damage = 10;
	
	public float range = 190;
	public Transform targetUnit;

	public float fireFrequency = .3f;

	public float fireFrequencyDeviation = .1f;

	void Start ()
	{
		StartCoroutine (CheckForNearUnits ());
	}

	IEnumerator CheckForNearUnits ()
	{
		yield return new WaitForSeconds (1);
		
		targetUnit = GetClosestUnits (UnitManager.Instance.AllEnemyUnits.ToArray ());
		
		if (targetUnit != null) {
			
			if (Vector3.Distance (transform.position, targetUnit.position) < range) {
				
				GetComponent<LineRenderer> ().SetPosition (0, transform.position);
				GetComponent<LineRenderer> ().SetPosition (1, targetUnit.position);
			
				StartCoroutine (shortLaserBurst ());
				
				if (targetUnit != null) {
					if (targetUnit.GetComponent<UnitStats> () != null) {


						targetUnit.GetComponent<UnitStats> ().DoDamage (damage);	
					
					} else {
						GetComponent<LineRenderer> ().enabled = false;
					}
				} else {
					GetComponent<LineRenderer> ().enabled = false;
				}
			} else {
				GetComponent<LineRenderer> ().enabled = false;
			}
		}
		StartCoroutine (CheckForNearUnits ());
	}

	IEnumerator shortLaserBurst ()
	{
		GetComponent<LineRenderer> ().enabled = true;
		yield return new WaitForSeconds (fireFrequency + Random.Range (-fireFrequencyDeviation, fireFrequencyDeviation));
		GetComponent<LineRenderer> ().enabled = false;
	}

	Transform GetClosestUnits (Transform[] enemies)
	{
		Transform tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (Transform t in enemies) {
			if (t != null) {
				float dist = Vector3.Distance (t.position, currentPos);
				if (dist < minDist) {
					tMin = t;
					minDist = dist;
				}
			}
		}
		return tMin;
	}
}

﻿using UnityEngine;
using System.Collections;

public class TurretPhaser : MonoBehaviour
{

	public Transform phaserPrefab;

	public float damage = 10;
	
	public float range = 190;
	public Transform targetUnit;
	
	public float fireFrequency = .3f;
	
	public float fireFrequencyDeviation = .1f;
	
	void Start ()
	{
		StartCoroutine (CheckForNearUnits ());
	}
	
	IEnumerator CheckForNearUnits ()
	{
		yield return new WaitForSeconds (1);
		
		targetUnit = GetClosestUnits (UnitManager.Instance.AllEnemyUnits.ToArray ());
		
		if (targetUnit != null) {
			
			if (Vector3.Distance (transform.position, targetUnit.position) < range) {
					
				StartCoroutine (shortPhaserBurst ());
				

			}
		}
		StartCoroutine (CheckForNearUnits ());
	}
	
	IEnumerator shortPhaserBurst ()
	{
		transform.LookAt (targetUnit);


		Transform handle = Instantiate (phaserPrefab, transform.position, transform.rotation) as Transform;

		//	GetComponent<LineRenderer> ().enabled = true;
		yield return new WaitForSeconds (fireFrequency + Random.Range (-fireFrequencyDeviation, fireFrequencyDeviation));
		//	GetComponent<LineRenderer> ().enabled = false;
	}
	
	Transform GetClosestUnits (Transform[] enemies)
	{
		Transform tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (Transform t in enemies) {
			if (t != null) {
				float dist = Vector3.Distance (t.position, currentPos);
				if (dist < minDist) {
					tMin = t;
					minDist = dist;
				}
			}
		}
		return tMin;
	}
}

using UnityEngine;
using System.Collections;

public class TurretMiner : MonoBehaviour
{
	public Transform targetAsteroid;

	public Transform currentWaypointTarget;

	public float minedOre = 0;

	public float maxOre = 10;

	public bool isMining = false;


	void Start ()
	{
		StartCoroutine (CheckForNearAsteroids ());
		transform.parent.GetComponent<UnitMovement> ().tMiner = transform;
	}


	public void DumpOre ()
	{
		//if (transform.parent.GetComponent<UnitMovement> ().mineSequence == UnitMovement.MineSequence.GODOCK) {
		ResourceManager.Instance.Ore += minedOre;
		minedOre = 0;
		//}
	}

	public void AddOre (float ore)
	{
	
		
		if (minedOre < maxOre) {
			minedOre += ore;

			//Debug.Log ("mined ore: " + minedOre);
		} else {

			GetComponent<LineRenderer> ().enabled = false;
			//transform.parent.GetComponent<UnitMovement> ().isMining = false;
			transform.parent.GetComponent<UnitMovement> ().mineSequence = UnitMovement.MineSequence.GOOUT;
			StopCoroutine (CheckForNearAsteroids ());
		}

	}

	IEnumerator CheckForNearAsteroids ()
	{
	

		yield return new WaitForSeconds (1);


		
		targetAsteroid = GetClosestAsteroid (ResourceManager.Instance.Asteroids.ToArray ());

		transform.parent.GetComponent<UnitMovement> ().currentAsteroid = targetAsteroid;

		if (targetAsteroid != null) {
			
			if (Vector3.Distance (transform.position, targetAsteroid.position) < 50) {

				GetComponent<LineRenderer> ().SetPosition (0, transform.position);
				GetComponent<LineRenderer> ().SetPosition (1, targetAsteroid.position);
			
				if (targetAsteroid != null) {

					if (transform.parent.GetComponent<UnitMovement> ().mineSequence != UnitMovement.MineSequence.GOOUT) {
						transform.parent.GetComponent<UnitMovement> ().isMining = true;
						transform.parent.GetComponent<UnitMovement> ().mineSequence = UnitMovement.MineSequence.GOMINE;
					}
					if (targetAsteroid.GetComponent<Asteroid> () != null) {
					
						targetAsteroid.GetComponent<Asteroid> ().DepleteOre (1, transform);

						if (targetAsteroid.GetComponent<Asteroid> ().unitsOre < 1) {
							GetComponent<LineRenderer> ().enabled = false;
							transform.parent.GetComponent<UnitMovement> ().isMining = false;
						} else {
							GetComponent<LineRenderer> ().enabled = true;
						}
					} else {
						Debug.LogError ("script is missing");
						transform.parent.GetComponent<UnitMovement> ().isMining = false;
					}
				} else {
					Debug.LogError ("asteroid is missing");
					transform.parent.GetComponent<UnitMovement> ().isMining = false;
				}
			} else {
				GetComponent<LineRenderer> ().enabled = false;
			}
		} else {
			transform.parent.GetComponent<UnitMovement> ().isMining = false;
			GetComponent<LineRenderer> ().enabled = false;
		}

		StartCoroutine (CheckForNearAsteroids ());
	}

	Transform GetClosestAsteroid (Transform[] stroids)
	{
		Transform tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (Transform t in stroids) {
			if (t != null) {
				float dist = Vector3.Distance (t.position, currentPos);
				if (dist < minDist) {
					tMin = t;
					minDist = dist;
				}
			}
		}
		return tMin;
	}
}

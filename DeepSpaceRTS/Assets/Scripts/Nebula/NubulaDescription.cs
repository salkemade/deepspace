﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using System.Collections.Generic;

public class NubulaDescription : MonoBehaviour
{

	public List<Transform> nebulaList = new List<Transform> ();

	public List<RectTransform> nebulaRecList = new List<RectTransform> ();


	void FixedUpdate ()
	{
		int i = 0;
		foreach (Transform t in nebulaList) {

			Vector3 screenPos = Camera.main.WorldToScreenPoint (t.position);
			//	Debug.Log ("target is " + screenPos.x + " pixels from the left");

			nebulaRecList [i].position = new Vector2 (screenPos.x, screenPos.y);

			i++;
		}

	}


	/*
	void OnGUI ()
	{

		int i = 0;
		foreach (Transform t in nebulaList) {

			Vector3 screenPos = Camera.main.WorldToScreenPoint (t.position);
			Debug.Log ("target is " + screenPos.x + " pixels from the left");

			//	nebulaRecList [i].anchoredPosition = new Vector2 (screenPos.x, screenPos.y);

			GUI.Box (new Rect (screenPos.x, screenPos.y, 300, 100), "BLIEP BLIEP");

			i++;
		}
	}
	*/
}

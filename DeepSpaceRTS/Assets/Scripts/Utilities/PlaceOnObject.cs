﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlaceOnObject : MonoBehaviour
{
	public Transform target;

	public  Rect screenRect;


	void Start ()
	{
		screenRect = new Rect (0, 0, Screen.width, Screen.height);
	}


	void LateUpdate ()
	{
		Vector3 point = Camera.main.WorldToScreenPoint (target.position);

		if (screenRect.Contains (new Vector2 (point.x, point.y))) {
			transform.position = Camera.main.WorldToScreenPoint (target.position);
		}
	}
}

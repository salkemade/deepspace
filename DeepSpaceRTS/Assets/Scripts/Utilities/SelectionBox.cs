﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SelectionBox : MonoBehaviour
{
	public static SelectionBox Instance;

	public List<Transform> selectedObjects = new List<Transform> ();

	// Update is called once per frame
	// Draggable inspector reference to the Image GameObject's RectTransform.
	public RectTransform selectionBox;
	
	// This variable will store the location of wherever we first click before dragging.
	private Vector2 initialClickPosition = Vector2.zero;

	public LayerMask mask;

	Rect selRec = new Rect ();

	int screenBorderMargin;

	void Awake ()
	{
		if (Instance != null) {
			Debug.LogWarning ("Selection Box instance already exists");
		} else {
			Instance = this;
		}
	}

	enum selectionBoxStates
	{
		NONE,
		DRAGGING,
		SELECTING,
		CANCELED,
		START,
		STOPPED

	}
	;

	selectionBoxStates CurrentSate;

	void ClearSelectedObjects ()
	{
		selectedObjects.Clear ();
	}


	void Start ()
	{
		ClearSelectedObjects ();
		screenBorderMargin = SpaceRTScamera.Instance.screenBorderMargin;
	}
	
	void DeSelectUnits ()
	{
		if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject (-1)) {
			foreach (Transform t in UnitManager.Instance.SelectedUnits) {
			
				t.GetComponent<Selected> ().DeSelect ();
			
			}
			UnitManager.Instance.SelectedUnits.Clear ();
		}
	}
	
	
	void Update ()
	{

		if (Input.mousePosition.x < screenBorderMargin || Input.mousePosition.x > Screen.width - screenBorderMargin || Input.mousePosition.y < screenBorderMargin || Input.mousePosition.y > Screen.height - screenBorderMargin) {

			selectionBox.sizeDelta = Vector2.zero;
			return;
		}




		//	if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject (-1)) {
		//	} else {
		// Click somewhere in the Game View.
		if (Input.GetMouseButtonDown (0)) {

			// Get the initial click position of the mouse. No need to convert to GUI space
			// since we are using the lower left as anchor and pivot.
			initialClickPosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			
			// The anchor is set to the same place.
			selectionBox.anchoredPosition = initialClickPosition;

			Ray ray;
			RaycastHit hit;
			
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (!Physics.Raycast (ray, out hit, 10000, mask)) {
				
				Debug.Log ("should deselect");
				DeSelectUnits ();
			} else {
				Debug.Log ("should  select");

				if (hit.transform.tag == "PlayerUnit") {
					if (hit.transform.GetComponent<Selected> ().isSelected) {
						hit.transform.GetComponent<Selected> ().DeSelect ();
						UnitManager.Instance.SelectedUnits.Remove (hit.transform);
					} else {
						if (!UnitManager.Instance.SelectedUnits.Contains (hit.transform)) {
							hit.transform.GetComponent<Selected> ().Select ();
							UnitManager.Instance.SelectedUnits.Add (hit.transform);
						}
					}
				} else {

				}
			}
		}

		// While we are dragging.
		if (Input.GetMouseButton (0)) {
			// Store the current mouse position in screen space.
			Vector2 currentMousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			
			// How far have we moved the mouse?
			Vector2 difference = currentMousePosition - initialClickPosition;
			
			// Copy the initial click position to a new variable. Using the original variable will cause
			// the anchor to move around to wherever the current mouse position is,
			// which isn't desirable.
			Vector2 startPoint = initialClickPosition;
			
			// The following code accounts for dragging in various directions.
			if (difference.x < 0) {
				startPoint.x = currentMousePosition.x;
				difference.x = -difference.x;
			}
			if (difference.y < 0) {
				startPoint.y = currentMousePosition.y;
				difference.y = -difference.y;
			}
			
			// Set the anchor, width and height every frame.
			selectionBox.anchoredPosition = startPoint;
			selectionBox.sizeDelta = difference;

			selRec.Set (selectionBox.anchoredPosition.x, selectionBox.anchoredPosition.y, selectionBox.sizeDelta.x, selectionBox.sizeDelta.y);

			foreach (Transform t in UnitManager.Instance.AllSelectableUnits) {
				if (t != null) {
					if (selRec.Contains (Camera.main.WorldToScreenPoint (t.position))) {

						// alleen geadd if they don't exist yet

						if (UnitManager.Instance.SelectedUnits.Contains (t)) {

						} else {
							t.GetComponent<Selected> ().Select ();
							UnitManager.Instance.SelectedUnits.Add (t);
						}
					}

				} else {
					UnitManager.Instance.SelectedUnits.Remove (t);
				}
			}
		}


		// After we release the mouse button.
		if (Input.GetMouseButtonUp (0)) {
			// Reset
			initialClickPosition = Vector2.zero;
			selectionBox.anchoredPosition = Vector2.zero;
			selectionBox.sizeDelta = Vector2.zero;

			selRec.height = 0;
			selRec.width = 0;
		}

	}
//	}

}

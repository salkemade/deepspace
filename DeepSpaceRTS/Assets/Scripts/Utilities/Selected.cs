﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;

public class Selected : MonoBehaviour
{
	public bool isSelected = false;

	public void Select ()
	{
		Debug.Log ("select");
		isSelected = true;
		GetComponent<UnitStats> ().status.gameObject.SetActive (true);
	}

	public void DeSelect ()
	{
		Debug.Log ("deselect");
		isSelected = false;
		GetComponent<UnitStats> ().status.gameObject.SetActive (false);
	}
}

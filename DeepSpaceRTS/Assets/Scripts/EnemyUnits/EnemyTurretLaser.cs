﻿using UnityEngine;
using System.Collections;

public class EnemyTurretLaser : MonoBehaviour
{

	public float damage = 10;

	public float range = 150;
	public Transform targetUnit;
	
	void Start ()
	{
		StartCoroutine (CheckForNearUnits ());
	}
	
	
	IEnumerator CheckForNearUnits ()
	{
		
		yield return new WaitForSeconds (1);
		
		targetUnit = GetClosestUnits (UnitManager.Instance.AllSelectableUnits.ToArray ());
		
		if (targetUnit != null) {
			
			if (Vector3.Distance (transform.position, targetUnit.position) < range) {
				
				GetComponent<LineRenderer> ().SetPosition (0, transform.position);
				GetComponent<LineRenderer> ().SetPosition (1, targetUnit.position);

				StartCoroutine (shortLaserBurst ());


				if (targetUnit != null) {
					if (targetUnit.GetComponent<UnitStats> () != null) {
						targetUnit.GetComponent<UnitStats> ().DoDamage (damage);	
					} else {

					}
				} else {

				}
			} else {
				GetComponent<LineRenderer> ().enabled = false;
			}
		}
		
		StartCoroutine (CheckForNearUnits ());
	}

	IEnumerator shortLaserBurst ()
	{
		GetComponent<LineRenderer> ().enabled = true;
		yield return new WaitForSeconds (.3f);
		GetComponent<LineRenderer> ().enabled = false;
	}
	
	
	Transform GetClosestUnits (Transform[] enemies)
	{
		Transform tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (Transform t in enemies) {
			if (t != null) {
				float dist = Vector3.Distance (t.position, currentPos);
				if (dist < minDist) {
					tMin = t;
					minDist = dist;
				}
			}
		}
		return tMin;
	}




}

﻿using UnityEngine;
using System.Collections;

public class EnemyUnitMovement : MonoBehaviour
{


	public float range = 150;
	public Transform targetUnit;

	void Start ()
	{
		StartCoroutine (CheckForNearUnits ());
	}


	IEnumerator CheckForNearUnits ()
	{
		
		yield return new WaitForSeconds (1);
		
		targetUnit = GetClosestUnits (UnitManager.Instance.AllSelectableUnits.ToArray ());
		
		if (targetUnit != null) {
			
			if (Vector3.Distance (transform.position, targetUnit.position) < range) {
				
				//do a manouvre
				
				
			} else {
				//	GetComponent<LineRenderer> ().enabled = false;
			}
		}
		
		StartCoroutine (CheckForNearUnits ());
	}

	Transform GetClosestUnits (Transform[] enemies)
	{
		Transform tMin = null;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		foreach (Transform t in enemies) {
			if (t != null) {
				float dist = Vector3.Distance (t.position, currentPos);
				if (dist < minDist) {
					tMin = t;
					minDist = dist;
				}
			}
		}
		return tMin;
	}


	void OnDestroy ()
	{
		UnitManager.Instance.AllEnemyUnits.Remove (transform);
	}

}

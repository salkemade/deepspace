﻿using UnityEngine;
using System.Collections;

public class EnemyStatus : MonoBehaviour
{
	public Transform statusBar;

	public void ShowStatusBar ()
	{
		if (statusBar != null)
			statusBar.gameObject.SetActive (true);
	}

	public void HideStatusBar ()
	{
		if (statusBar != null)
			statusBar.gameObject.SetActive (false);
	}

	void OnMouseEnter ()
	{
		if (statusBar != null)
			ShowStatusBar ();
	}

	void OnMouseExit ()
	{
		//	if (Input.GetMouseButtonDown (0)) {
		if (statusBar != null)
			HideStatusBar ();
		//	}
	}
}

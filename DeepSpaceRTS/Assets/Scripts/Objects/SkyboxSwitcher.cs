﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkyboxSwitcher : MonoBehaviour
{

	public List<Material> skyboxList = new List<Material> ();

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (SwitchLoop ());
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		transform.Rotate (0, .1f, 0);
	}


	IEnumerator SwitchLoop ()
	{
		yield return new WaitForSeconds (Random.Range (3, 7));
		GetComponent<GlitchEffect> ().enabled = true;
		yield return new WaitForSeconds (Random.Range (3, 7));

		GetComponent<Skybox> ().material = skyboxList [Random.Range (0, skyboxList.Count)];
		yield return new WaitForSeconds (Random.Range (3, 7));
		GetComponent<GlitchEffect> ().enabled = false;

		yield return new WaitForSeconds (Random.Range (3, 7));
		StartCoroutine (SwitchLoop ());
	}

}

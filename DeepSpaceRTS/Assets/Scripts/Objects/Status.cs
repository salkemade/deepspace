﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Status : MonoBehaviour
{
	public Transform owner;
		
	public bool isCarrier = false;

	void Start ()
	{
		GetComponent<PlaceOnObject> ().target = owner;
		//transform.parent = StatusManager.Instance.transform;
		transform.SetParent (StatusManager.Instance.transform);

		StatusManager.Instance.statusList.Add (transform);
		gameObject.SetActive (false);
		if (isCarrier) {
			BuildMenu.Instance.ShowMenu ();
		}

	}
	
	void Update ()
	{
		
	}
}

﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
	public bool isPaused = false;

	void OnGUI ()
	{

		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (isPaused) {
				//	Time.timeScale = 1;
				Debug.Log ("paused");

				isPaused = true;
			} else {
				Debug.Log ("un paused");

				//	Time.timeScale = 0;
				isPaused = false;
			}
		}
		if (isPaused) {
			if (GUI.Button (new Rect (Screen.width - 100, Screen.height, 200, 30), "Continue")) {
				Time.timeScale = 1;
				isPaused = false;
			}
		}

	}
}

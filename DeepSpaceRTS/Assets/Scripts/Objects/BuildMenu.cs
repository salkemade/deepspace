using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuildMenu : MonoBehaviour
{
	public static BuildMenu Instance;

	public Transform buildMenu;

	public List<Transform> prefabList = new List<Transform> ();

	public Transform dockingbay;
	public Transform waypointOut;

	bool building = false;

	void Awake ()
	{
		if (Instance != null) {
			Debug.LogWarning ("instance of buildmenu already exists");
		} else {
			Instance = this;
		}
	}

	public List<Transform> loadingBar = new List<Transform> ();

	public void ShowMenu ()
	{
		buildMenu.gameObject.SetActive (true);
	}

	public void HideMenu ()
	{
		buildMenu.gameObject.SetActive (false);
	}

	public void BuildShip (int index)
	{
		int cost = 0;
		switch (index) {
		case 0:
			cost = 5;
			break;
		case 1:
			cost = 20;
			break;

		}



		if (ResourceManager.Instance.Ore > cost) {
			ResourceManager.Instance.Ore -= cost;

			StartCoroutine (BuildShipRoutine (index));
		} else {
			Debug.Log ("insufficient funds");
		}
	
	}

	IEnumerator BuildShipRoutine (int index)
	{
		//loading bar routine
		if (!building) {
			building = true;
			loadingBar [index].GetComponent<Image> ().fillAmount = 0;

			for (int i = 0; i < 100; i++) {
				loadingBar [index].GetComponent<Image> ().fillAmount += .01f;
				yield return null;
			}
		


			yield return  new WaitForSeconds (1);
			Transform ship = Instantiate (prefabList [index], dockingbay.position, dockingbay.rotation) as Transform;
			UnitManager.Instance.AllSelectableUnits.Add (ship);

			ship.GetComponent<UnitStats> ().status.rotation = Quaternion.identity;

			loadingBar [index].GetComponent<Image> ().fillAmount = 0;

			ship.GetComponent<UnitMovement> ().target = waypointOut;
			ship.GetComponent<UnitMovement> ().state = UnitMovement.States.MOVINGTOPOINT;

			building = false;
		}

	}


	void Update ()
	{

	}


}

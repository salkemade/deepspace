﻿using UnityEngine;
using System.Collections;

public class DebugSystem : MonoBehaviour
{

	bool debug = false;

	public Transform horizontalPlane;
	public Transform verticalPlane;
	// Use this for initialization
	void Start ()
	{
		DebugOff ();
	}

	void DebugOn ()
	{
		horizontalPlane.GetComponent<Renderer> ().enabled = true;
		verticalPlane.GetComponent<Renderer> ().enabled = true;
	}

	void DebugOff ()
	{
		horizontalPlane.GetComponent<Renderer> ().enabled = false;
		verticalPlane.GetComponent<Renderer> ().enabled = false;
	}

	public void ToggleDebug ()
	{
		if (debug) {
			DebugOff ();
		} else {
			DebugOn ();
		}

		debug = !debug;
	}

	// Update is called once per frame
	void Update ()
	{
	
		if (Input.GetKeyDown (KeyCode.D)) {
			ToggleDebug ();
		}
			

	}





}

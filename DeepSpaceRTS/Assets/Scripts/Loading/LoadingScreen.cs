﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (LoadNextLevel ());
	}


	IEnumerator LoadNextLevel ()
	{
		yield return new WaitForSeconds (3);

		SceneManager.LoadScene (2);
	}

}
